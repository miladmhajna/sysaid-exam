result = 0
resultArray = []
number = '';

$(function() {
    let board = $('#board');

    //init
    board.text(result)

    function add(char) {
        resultArray.push(char)
    }

    function remove() {
        result = result.slice(0, -1)
        resultArray.pop()
        board.text(result)
    }

    function reset() {
        resultArray = []
        result = 0
        board.text(result)
    }

    function updateBoard(char) {
        result += char
        board.text(result)
    }

    function calculate() {
    
        const url = './../calculate.php'

        $.post( url, { result }, function( data ) {
            resultArray = [data]
            result = data
            board.text(data)
        }, 'json');
    }

    /// on click on a AC
    $('.btn-ac').click(function() {
        reset();
    });


    /// on click on an operation
    $('.btn-operation').click(function() {
        const operation = $(this).attr('data-op')

        
        //add number
        if(!isNaN(number) && number != '') {
            add(number)
            number = ''
        }

        if(resultArray.length) {
            const lastDigit = resultArray[resultArray.length - 1]

            // check if last digit is a number 
            if(!isNaN(lastDigit)) {
                add(operation)
            } else {
                remove()
                add(operation)
            }

        } else {
            add(operation)
        }

        updateBoard(operation)

    })

    /// on click on a number
    $('.btn-number').click(function() {
        const digit = $(this).attr('data-op')
        number += digit

        if(result === 0)
            result = digit
        else
            result += digit

        board.text(result)
    })

                /// on click on a number
    $('.btn-result').click(function() {
        if(isNaN(number) || number == '')
            return

        //add number
        add(number)
        number = ''

        calculate()
    })
})
